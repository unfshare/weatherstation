//
//  CityWeatherControllerTests.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/27/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import XCTest

@testable import WeatherStation

class CityWeatherControllerTests: XCTestCase {
    
    let controller = CityWeatherController()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTimeParameters() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let time = Date(timeIntervalSince1970: 1503318645)
        
        let result = controller.timeParameters(time)
        
        let expectedStartTime: Double = 1503316800
        let expectedEndTime: Double = 1503360000
        
        XCTAssert(result.0.timeIntervalSince1970 == expectedStartTime, "Expected start time: \(expectedStartTime), got \(result.0.timeIntervalSince1970)")
        XCTAssert(result.1.timeIntervalSince1970 == expectedEndTime, "Expected end time: \(expectedEndTime), got \(result.1.timeIntervalSince1970)")
    }
    
    func testRequestToGetWeatherInfo() {
        
    }
    
}
