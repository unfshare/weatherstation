//
//  CityWeatherTableViewCell.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/27/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import UIKit

class CityWeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblWeatherIcon: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblAirPressure: UILabel!
    @IBOutlet weak var lblWindSpeed: UILabel!
    @IBOutlet weak var lblWindDirection: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(_ data: WeatherInfo) {
        lblDate.text = data.time?.toString(format: "E").uppercased()
        lblTime.text = data.time?.toString(format: "HH:mm")
        lblTemperature.text = "\(Int(data.temperature!))°C"
        
        lblHumidity.setWeatherInfo(value: Int(data.humidity!), icon: "\u{f07a}", bold: false)
        lblAirPressure.setWeatherInfo(value: Int(data.pressure!), icon: "\u{f079}", bold: false)
        lblWindSpeed.setWeatherInfo(value: Int(data.windSpeed!), icon: "\u{f050}", bold: false)
        lblWindDirection.setWeatherInfo(value: "\(Int(data.windDirection!))°", icon: "\u{f057}", bold: false)
    }
}
