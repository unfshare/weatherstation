//
//  CityWeatherController.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/9/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation
import Alamofire

class CityWeatherController {
    
    init() {
        
    }
    
    func requestToGetWeatherInfo(_ city: String, completion handler: @escaping (WeatherInfoResponse?, Error?) -> Void) {
        
        let now = Date()
        let (startTime, endTime) = timeParameters(now)
        
        let parameters = [
            FMIRequestParameterName.request: "getFeature",
            FMIRequestParameterName.storedQueryId: "fmi::forecast::hirlam::surface::point::simple",
            FMIRequestParameterName.place: city,
            
            // Mark: WARNING: The server will return the information which follows exact the order of parameters
            // Therefore, need to parse the response data same as the order of the request parameters here.
            FMIRequestParameterName.parameters: "temperature,humidity,windspeedms,winddirection,pressure,totalcloudcover,radiationglobalaccumulation,precipitation1h",
            FMIRequestParameterName.startTime: startTime.toString(format: AppConstants.timeFormat),
            FMIRequestParameterName.endTime: endTime.toString(format: AppConstants.timeFormat),
            FMIRequestParameterName.timeStep: "60",
        ]
        
        HTTPClient.shared.requestGET(parameters: parameters) { (result: WeatherInfoResponse?, error: NSError?) in
            handler(result, error)
        }
    }
    
    func timeParameters(_ time: Date) -> (Date, Date) {
        let gregorian = Calendar(identifier: .gregorian)
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: time)
        
        components.minute = 0
        components.second = 0
        
        let startTime = gregorian.date(from: components)!
        
        components.hour = components.hour! + 24
        
        let endTime = gregorian.date(from: components)!
        return (startTime, endTime)
        
    }
    
}
