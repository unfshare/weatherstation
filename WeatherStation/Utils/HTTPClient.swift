//
//  HTTPClient.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/8/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

protocol WSErrorProtocol: Error {
    
    var title: String { get }
    var description: String { get }
    var code: Int { get }
}

struct WSError: WSErrorProtocol {
    
    var title: String
    var description: String
    var code: Int
    
    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self.description = description
        self.code = code
    }
}

protocol WSResponseObjectSerializable {
    init?(response: HTTPURLResponse, representation: XMLIndexer)
}

extension DataRequest {
    func responseXMLObjects<T: WSResponseObjectSerializable>(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<T>) -> Void)
        -> Self
    {
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                let parserError = WSError(title: "life.coder.WeatherStation", description: "Data is NULL", code: -1)
                return .failure(parserError)
            }
            
            let xml = SWXMLHash.parse(data)
            guard let response = response, let responseObject = T(response: response, representation: xml) else {
                let parserError = WSError(title: "life.coder.WeatherStation", description: "XML could not be serialized: \(xml)", code: -2)
                return .failure(parserError)
            }
            
            return .success(responseObject)
        }
        
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}


class HTTPClient {
    
    //MARK: Local Variable
    private let baseURL = "http://data.fmi.fi/fmi-apikey/\(AppConstants.kFMIApiKey)/wfs"
    private var sessionManager: SessionManager!
    
    static let shared : HTTPClient = {
        let instance = HTTPClient()
        return instance
    }()
    
    //MARK: Init method

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 60 // seconds
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    //MARK: Public methods
    
    public func request(method: HTTPMethod, parameters: [String: Any]?, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest {
        return sessionManager.request(baseURL,
                                      method: method,
                                      parameters: parameters,
                                      encoding: encoding,
                                      headers: headers)
    }
    
    public func requestGET<T: WSResponseObjectSerializable>(parameters: [String: Any]?, handler: @escaping (T?, NSError?) -> Void) {
        _ = self.request(method: HTTPMethod.get,
                         parameters: parameters,
                         encoding: URLEncoding.queryString)
        .responseXMLObjects { (response: DataResponse<T>) in
            handler(response.result.value, nil)
        }
    }
    
}
