//
//  LocationHelper.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 9/4/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationHelperDelegate {
    func didUpdateUserLocation(_ locality: String?, postalCode: String?, area: String?, country: String?)
    func didFailToUpdateUserLocation(_ error: Error?)
}

class LocationHelper: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var location: CLLocation?
    
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    
    var city: String?
    var country: String?
    var countryShortName: String?
    
    var delegate: LocationHelperDelegate?
    
    static let shared : LocationHelper = {
        let instance = LocationHelper()
        return instance
    }()
    
    override init() {
        super.init()
    }
    
    public func startLocationManager(_ delegate: LocationHelperDelegate) {
        // always good habit to check if locationServicesEnabled
        self.delegate = delegate
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    public func stopLocationManager() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        
        self.delegate = nil
    }
    
    // MARK: CLLocationManagerDelegate methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
            if let _ = error {
                loggingPrint("Reverse geocoder failed with error" + error!.localizedDescription)
                self.delegate?.didFailToUpdateUserLocation(error)
                return
            }
            
            if let _ = placemarks, placemarks!.count > 0 {
                let placemark = placemarks![0] as CLPlacemark
                self.delegate?.didUpdateUserLocation(placemark.locality,
                                                postalCode: placemark.postalCode,
                                                area: placemark.administrativeArea,
                                                country: placemark.country)
            } else {
                loggingPrint("Problem with the data received from geocoder")
                self.delegate?.didFailToUpdateUserLocation(nil)
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        loggingPrint("Error while updating location " + error.localizedDescription)
        self.delegate?.didFailToUpdateUserLocation(error)
    }
}
