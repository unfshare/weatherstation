//
//  Extensions.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/25/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func toString(format: String, timeZone: String? = nil) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if let _ = timeZone {
            formatter.timeZone = TimeZone(abbreviation: timeZone!)
        }
        
        return formatter.string(from: self)
    }
    
    static func fromString(_ string: String, format: String, timeZone: String? = nil) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let date = formatter.date(from: string)
        return date
    }
}

extension UILabel {
    func setWeatherInfo(value: Any, icon: String, bold: Bool = false) {
        var robotoFont: UIFont!
        if bold {
            robotoFont = UIFont(name: "Roboto-Bold", size: self.font.pointSize)!
        } else {
            robotoFont = UIFont(name: "Roboto-Regular", size: self.font.pointSize)!
        }
        let valueString = NSMutableAttributedString(string: "\(value) ", attributes: [NSFontAttributeName: robotoFont!])
        let weatherFont = UIFont(name: "WeatherIcons-Regular", size: self.font.pointSize)!
        let iconString = NSAttributedString(string: icon, attributes: [NSFontAttributeName: weatherFont])
        valueString.append(iconString)
        
        self.attributedText = valueString
    }
}
