//
//  Constants.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/7/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation

struct AppConstants {
    static let kFMIApiKey = "e45def84-b2c5-4a58-8883-795f9b42e105"
    
    static let timeFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'" //2017-08-21T00:00:00Z
}

struct FMIRequestParameterName {
    static let request = "request"
    static let storedQueryId = "storedquery_id"
    // parameters (check possible parameters from server response by using stored query request without limitations, for example: &parameters=temperature,windspeedms)
    static let parameters = "parameters"
    // starttime (for example: &starttime=2013-02-28T20:00:00Z)
    static let startTime = "starttime"
    // endtime (for example: &endtime=2013-02-30T20:00:00Z)
    static let endTime = "endtime"
    // timestep (in minutes, for example: &timestep=40)
    static let timeStep = "timestep"
    static let geoId = "geoid"
    static let wmo = "wmo"
    static let fmiSid = "fmisid"
    static let place = "place"
    static let bbox = "bbox"
    // crs (check supported projections from getCapabilities-responnse, for example: &crs=EPSG::3067)
    static let csr = "csr"
}

struct CellIdentifier {
    static let cityWeather = "CityWeatherTableViewCell"
}
