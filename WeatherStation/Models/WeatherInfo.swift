//
//  WeatherInfo.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/27/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation

enum WeatherStatus: String {
    case sunny = "\u{f185}"
    case rainy = "\u{f0e9}"
    case cloudy = "\u{f0c2}"
    case clear = ""
}

struct WeatherInfo {
    var city: String
    var time: Date?
    var temperature: Float?                                 //XML member/element index: 0
    var humidity: Float?                                    //XML member/element index: 1
    var windSpeed: Float?                                   //XML member/element index: 2
    var windDirection: Float?                               //XML member/element index: 3
    var pressure: Float?                                    //XML member/element index: 4
    var totalCloudCover: Float?                             //XML member/element index: 5
    var radiationGlobalAccumulation: Float?                 //XML member/element index: 6
    var precipitation1h: Float?                             //XML member/element index: 7
    
    init(city: String) {
        self.city = city
    }
}
