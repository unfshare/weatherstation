//
//  WeatherInfo.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/9/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

struct WeatherInfoResponse: WSResponseObjectSerializable {
    
    var city: String?
    var data: [WeatherInfo]?
    
    init?(response: HTTPURLResponse, representation: XMLIndexer) {
        let members = representation["wfs:FeatureCollection"]["wfs:member"]
        if members.all.count == 0 {
            return
        }
        
        data = [WeatherInfo]()
        
        var info: WeatherInfo!
        for index in 0..<members.all.count {
            let member = members.all[index]
            
            let value = member["BsWfs:BsWfsElement"]["BsWfs:ParameterValue"].element
            
            if let text = value?.text {
                switch index % 8 {
                case 0:
                    info = WeatherInfo(city: "Helsinki")
                    info.temperature = Float(text)
                    
                    if let timeString = member["BsWfs:BsWfsElement"]["BsWfs:Time"].element?.text {
                        info.time = Date.fromString(timeString, format: AppConstants.timeFormat)
                    }
                case 1:
                    info.humidity = Float(text)
                case 2:
                    info.windSpeed = Float(text)
                case 3:
                    info.windDirection = Float(text)
                case 4:
                    info.pressure = Float(text)
                case 5:
                    info.totalCloudCover = Float(text)
                case 6:
                    info.radiationGlobalAccumulation = Float(text)
                case 7:
                    info.precipitation1h = Float(text)
                    data?.append(info)
                default:
                    break;
                }
            }
        }
    }
}
