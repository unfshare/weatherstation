//
//  BaseViewController.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/7/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func showAlert(title: String, message: String, button: String, actionHandler: ((UIAlertAction) -> Void)? = nil) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: button, style: .cancel) { (action) in
            actionHandler?(action)
        }
        
        alertView.addAction(alertAction)
        self.present(alertView, animated: true, completion: nil)
    }
}
