//
//  CityWeatherViewController.swift
//  WeatherStation
//
//  Created by Ngoc Tuan Le on 8/4/17.
//  Copyright © 2017 CoderLife. All rights reserved.
//

import UIKit

class CityWeatherViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, LocationHelperDelegate {
    
    private let controller = CityWeatherController()
    private var infos: [WeatherInfo]?
    private var city: String?
    
    @IBOutlet weak var tableView: UITableView!
    
    private var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let image = UIImage(named: "TopBarBanner")
        self.navigationItem.titleView = UIImageView(image: image)
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(red: 0.77, green: 0.34, blue: 0.18, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        // Add refresh control to table view
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        LocationHelper.shared.startLocationManager(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl.beginRefreshing()
        refreshData(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshData(_ refreshControl: UIRefreshControl) {
        if let _ = city {
            controller.requestToGetWeatherInfo(city!) { [weak self] response, error in
                if let strongSelf = self {
                    if let infos = response?.data {
                        strongSelf.infos = infos
                        strongSelf.tableView.reloadData()
                    } else if let _ = error {
                        strongSelf.showAlert(title: "Error", message: "Something went wrong. Please try again later.", button: "OK")
                    }
                    
                    strongSelf.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    // MARK: LocationHelperDelegate method
    
    func didUpdateUserLocation(_ locality: String?, postalCode: String?, area: String?, country: String?) {
        if let _ = locality, city != locality {
            city = locality
            refreshData(refreshControl)
        }
    }
    
    func didFailToUpdateUserLocation(_ error: Error?) {
        vActivityIndicator.stopAnimating()
        if let _ = error {
            self.showAlert(title: "Error",
                           message: "Can't get your current location. Please make sure that you already enabled location service and internet connection then try again.",
                           button: "OK") { [weak self] (action) in
                            if let strongSelf = self {
                                LocationHelper.shared.startLocationManager(strongSelf)
                            }
            }
            LocationHelper.shared.stopLocationManager()
        }
    }

    // MARK: Start UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let infos = self.infos {
            return infos.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.cityWeather, for: indexPath) as! CityWeatherTableViewCell
        let info = infos![indexPath.row]
        cell.setData(info)
        
        return cell
    }
}

